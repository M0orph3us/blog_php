<?php
class Articles
{
    private $_author;
    private $_title;
    private $_date_created;
    protected $_date_modified;

    public function __construct(string $title, string $author)
    {
        $this->_title = $title;
        $this->_author = $author;

        $this->hydrate([
            "title" => $title,
            "author" => $author
        ]);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    // Les Getters:
    public function getTitle()
    {
        return $this->_title;
    }

    public function getAuthor()
    {
        return $this->_author;
    }

    public function getDate_created()
    {
        return $this->_date_created;
    }

    // Les Setters:


    public function setDate_modified($date)
    {
        return $this->_date_modified = $date;
    }
}
