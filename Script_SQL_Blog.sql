#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: users
#------------------------------------------------------------

CREATE TABLE users(
        id_user  Int  Auto_increment  NOT NULL ,
        pseudo   Varchar (50) NOT NULL ,
        password Varchar (50) NOT NULL ,
        mail     Varchar (50) NOT NULL ,
        role     Varchar (255) NOT NULL
	,CONSTRAINT users_PK PRIMARY KEY (id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: articles
#------------------------------------------------------------

CREATE TABLE articles(
        id_article    Int  Auto_increment  NOT NULL ,
        auteur        Varchar (50) NOT NULL ,
        title         Varchar (50) NOT NULL ,
        content       Longtext NOT NULL ,
        date_created  Date NOT NULL ,
        date_modified TimeStamp NOT NULL ,
        nb_like       Int ,
        id_user       Int NOT NULL
	,CONSTRAINT articles_PK PRIMARY KEY (id_article)

	,CONSTRAINT articles_users_FK FOREIGN KEY (id_user) REFERENCES users(id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Comment
#------------------------------------------------------------

CREATE TABLE Comment(
        id_comment   Int  Auto_increment  NOT NULL ,
        auteur       Varchar (50) NOT NULL ,
        date_created Date NOT NULL ,
        comment      Longtext NOT NULL ,
        id_user      Int NOT NULL ,
        id_article   Int NOT NULL
	,CONSTRAINT Comment_PK PRIMARY KEY (id_comment)

	,CONSTRAINT Comment_users_FK FOREIGN KEY (id_user) REFERENCES users(id_user)
	,CONSTRAINT Comment_articles0_FK FOREIGN KEY (id_article) REFERENCES articles(id_article)
)ENGINE=InnoDB;

