<?php
class Users {
    private $_pseudo;
    private $_password;
    private $_mail;


    public function __construct(string $pseudo, mixed $password, string $mail) {
        $this -> _pseudo = $pseudo;
        $this -> _password = $password;
        $this -> _mail = $mail;

        $this->hydrate ([
            "pseudo" => $pseudo,
            "password" => $password,
            "mail" => $mail
        ]);
    }

    private function hydrate(array $array) {
        foreach($array as $key => $value) {
        $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
            $this->$method($value);
            }
        }
    }

    // Les Getters:
    public function getPseudo() {
        return $this->_pseudo;
    }

    // Les Setters:


}



?>