<?php
require "init.php";
$title = "Inscription";
require "elements/header.php";

$user = new UsersRepository();
$new = $user->newUser();
?>
<main class="main">
    
    <form style="display:flex; flex-direction:column; align-items:center;" action="#" method="POST">
        <h1>Inscription</h1>
        <label for="name">Nom d'utilisateur : </label>
        <input type="text" name="name" required>
        <label for="mail">Votre Mail : </label>
        <input type="text" name="mail" required>
        <label for="mdp">Mot de Passe : </label>
        <input type="text" name="mdp" required>
        <button style="margin-top:15px;">S'incrire</button>
    </form>
</main>
<?php
require "elements/footer.php"
?>